package week5.Assignmnet_Multitreading.reservation_question_05.system;

public class TicketCounter {

	private int availableSeats = 3;

	public  void bookTicket(String pname, int numberOfSeats) {
		synchronized(this){
		if ((availableSeats >= numberOfSeats) && (numberOfSeats > 0)) {
			System.out.println("Hi," + pname + " : " + numberOfSeats+ " Seats booked Successfully..");
			availableSeats = availableSeats- numberOfSeats;
			System.out.println("seat left "+availableSeats);
		} else {
			System.out.println("Hi," + pname + " : Seats Not Available");
			System.out.println("seat left " + availableSeats);
		}
	}}
}