package week5.Assignmnet_Multitreading;

public class Question04_odd implements Runnable{
    static void printOdd(int bound){
        for (int i=1;i<bound;i++){
            if(i%2!=0){
                System.out.println(i);
            }
        }
    }


    @Override
    public void run() {
        Question04_odd.printOdd(10);
    }

    public static void main(String[] args) {
        Question04_odd question04_odd=new Question04_odd();
        Thread thread=new Thread(question04_odd);
        thread.start();
    }
}
