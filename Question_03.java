package week5.Assignmnet_Multitreading;

public class Question_03 implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread());
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    public static void main(String[] args) throws InterruptedException {
        Question_03 bullets=new Question_03();
        Thread gun=new Thread(bullets);
        System.out.println(gun.getName());
        System.out.println(gun.getId());
        gun.setPriority(10);
        System.out.println(gun.getState());
        System.out.println(gun.isAlive());
        gun.start();
//        gun.join();
        System.out.println(gun.getPriority());
        System.out.println(gun.getState());
        System.out.println("***********************************************");

        Question_03 bullets2=new Question_03();
        Thread gun2=new Thread(bullets2);
        System.out.println(gun2.getName());
        System.out.println(gun2.getId());
        System.out.println(gun2.getState());
        gun2.setPriority(10);
        System.out.println(gun2.isAlive());
        gun2.start();
//        gun2.join();
        System.out.println(gun2.getPriority());
        System.out.println(gun2.getState());
        System.out.println("******************************************");

        Question_03 bullets3=new Question_03();
        Thread gun3=new Thread(bullets3);
        System.out.println(gun3.getName());
        System.out.println(gun3.getId());
        System.out.println(gun3.getState());
        gun3.setPriority(10);
        System.out.println(gun3.isAlive());
        gun3.start();
//        gun3.join();
        System.out.println(gun3.getPriority());
        System.out.println(gun3.getState());
        System.out.println("******************************");
    }
}
