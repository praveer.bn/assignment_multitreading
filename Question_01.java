package week5.Assignmnet_Multitreading;

public class Question_01 extends  Thread{
    public void run(){
        System.out.println("the details of running thread "+Thread.currentThread());
    }
    public static void main(String[] args) {
        Question_01 question_01=new Question_01();
question_01.setName("a");
        question_01.start();
//        question_01.start();

        question_01.run();
    }
}


/*
Question 1
we are illegal to the thread ,means the thread is already stared ,still we r calling start method on that
thread...

Exception in thread "main" java.lang.IllegalThreadStateException
	at java.lang.Thread.start(Thread.java:708)
	at week5.Assignmnet_Multitreading.Question_01.main(Question_01.java:11)
the details of running thread Thread[Thread-0,5,main]


Question 2

the details of running thread Thread[main,5,main]
the details of running thread Thread[Thread-0,5,main]

first the  formation of obj stack [run() explicitly)will create after that the start function will call run() implicitly
 */